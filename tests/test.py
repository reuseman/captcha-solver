import matplotlib.pyplot as plt
import numpy as np
from scipy import ndimage as ndi
from skimage.filters import sobel
from skimage.io import imread
from skimage.measure import label, regionprops
from skimage.morphology import watershed
from skimage.filters.rank import median
from skimage.morphology import disk


from managers.image_manager import ImageManager


# 84 - (5 * 0.7)

def get_splitted_segment(segment):
    # Sum every column
    sum_col = np.sum(segment, axis=0)
    # Distance between the middle
    width = segment.shape[1]
    middle_point = width // 2
    offset = (width - width * 0.9) // 2
    distances = [abs(middle_point - 1 - n) for n in range(0, width)]
    distances = np.multiply(distances, 0.7)
    sum_col = np.subtract(sum_col, distances)
    max = -1
    max_index = -1
    for i, value in enumerate(sum_col):
        if value > max:
            max = value
            max_index = i
    return max_index


def get_splitted_segment(segment):
    # Sum every column
    sum_col = np.sum(segment, axis=0)
    # Distance between the middle
    width = segment.shape[1]
    middle_point = width // 2
    offset = (width - width * 0.9) // 2
    distances = [abs(middle_point - 1 - n) for n in range(0, width)]
    distances = np.multiply(distances, 0.7)
    sum_col = np.subtract(sum_col, distances)
    max = -1
    max_index = -1
    for i, value in enumerate(sum_col):
        if value > max:
            max = value
            max_index = i
    return max_index


def get_segmented_image(image, erosion=False, debugstring=""):
    if erosion:
        image = ImageManager.get_erosioned_image(image)

    # Matrix of zeros with the same shape as the image, set value 1 if the original pixel is white, otherwise 2
    markers = np.zeros_like(image)
    markers[image > 0.1] = 1
    markers[image <= 0.1] = 2

    # Sobel use some sort of gradient and creates an elevation map that will be used to fill the different segments
    elevation_map = sobel(image)
    segmentation = watershed(elevation_map, markers)
    segmentation = ndi.binary_fill_holes(segmentation - 1)

    # Labeled is a matrix of the original image with 0 for the white, 1 to N for the different segments
    labeled = label(segmentation)

    cropped_images = []
    for region_index, region in enumerate(regionprops(labeled)):
        if region.area < 15:
            continue

        min_row, min_col, max_row, max_col = region.bbox
        width = max_col - min_col
        height = max_row - min_row

        # TODO try to remove width > height
        # if len(regionprops(labeled)) < 4 and width > height:
        #     middle_col = min_col + width // 2
        #     temp1 = np.copy(image[min_row:max_row, min_col:middle_col])
        #     temp2 = np.copy(image[min_row:max_row, middle_col:max_col])
        #     sub_labeled1 = labeled[min_row:max_row, min_col:middle_col]
        #     sub_labeled2 = labeled[min_row:max_row, middle_col:max_col]
        #
        #     temp1[sub_labeled1 != region_index + 1] = 1
        #     temp2[sub_labeled2 != region_index + 1] = 1
        #     cropped_images.append((temp1, min_col))
        #     cropped_images.append((temp2, middle_col))
        # else:
        temp = np.copy(image[min_row:max_row, min_col:max_col])
        sub_labeled = labeled[min_row:max_row, min_col:max_col]

        # Every pixel that does not belong to the current region will be sett as white (other letters that enter in
        # the current region)
        temp[sub_labeled != region_index + 1] = 1

        # temp = resize(temp, (40, 40), anti_aliasing=True)
        # The tuple contains the image and the min_col, the reason why for the latter is that is needed to sort
        # and don't lost the mapping in the dataset between captcha and the relative solution
        cropped_images.append((temp, min_col))

    # Sort by colmin and then remove the tuple (image, colmin) and obtain only image as element of the list
    cropped_images.sort(key=lambda cropped: cropped[1])
    return list(map(lambda value: value[0], cropped_images))


def print_segments(image_segments, index_line=None):
    # index_line = (3, 59) means iteration 3 and x cordinate 59)
    n = len(image_segments)
    fig, ax_lst = plt.subplots(1, n)

    for i in range(0, n):
        ax_lst[i].imshow(image_segments[i], cmap="gray", interpolation="nearest")
        ax_lst[i].set_title("segment" + str(i))
        ax_lst[i].axis('on')
        if index_line and index_line[0] == i:
            ax_lst[i].axvline(x=index_line[1])

    plt.show()

    #     ax[i+1].set_title("segment" + str(i+1))
    #     ax[i+1].axis('on')
    #
    #     fig.tight_layout()


images = {
    1: imread("/home/alex/Development/python/captcha_solver/dataset/binarized/rwwy.jpg", True),
    3: imread("/home/alex/Development/python/captcha_solver/dataset/binarized/eyyq.jpg", True),
    6: imread("/home/alex/Development/python/captcha_solver/dataset/binarized/nzsb.jpg", True)
}

print(len(get_segmented_image(imread("/home/alex/Development/python/captcha_solver/dataset/binarized/aggs.jpg", True))))

for key in images:
    image2 = images.get(key)
    segments = get_segmented_image(image2)

    print_segments(segments)

    if len(segments) < 4:
        print("LUNGHEZZA SEGMENTI", len(segments))
        index_greater_width = -1
        greater_width = 0
        for i, seg in enumerate(segments):
            if seg.shape[1] > greater_width:
                greater_width = seg.shape[1]
                index_greater_width = i

        print(type(segments[index_greater_width]))
        x = get_splitted_segment(segments[index_greater_width])
        new_segments = []
        plus = 0
        for i in range(0, 3):
            if i != index_greater_width:
                new_segments.append(segments[i])
            else:
                plus = 1
                image1 = segments[i][:, :x]
                image2 = segments[i][:, x:]

                new_segments.append(median(image1, disk(1)))
                new_segments.append(image1)

        print_segments(new_segments)
        print_segments(segments, index_line=(index_greater_width, x))
        print("INDEX", x)
