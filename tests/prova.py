import matplotlib.pyplot as plt
 
from skimage.filters import threshold_minimum
from skimage.color import rgb2gray
import numpy as np
from skimage.io import imread
import matplotlib.image as mpimg
 
from image_manager import ImageManager
from commons import Commons
 
from os.path import join
 
def count_nonblack_np(img):
    """Return the number of pixels in img that are not black.
    img must be a Numpy array with colour values along the last axis.
 
    """
    return img.any(axis=-1).sum()
 
 
 
grayscale_image = imread(join(Commons.original_dataset_path,'aggs.jpg'), True)
mpimg.imsave(join(Commons.original_dataset_path,'prova.jpg'), grayscale_image, cmap="gray")
x = imread(join(Commons.original_dataset_path,'prova.jpg'), True)
 
fig = plt.figure()
plt.imshow(x, cmap="gray")
plt.show()
 
# fig = plt.figure(figsize=(3, 1))  # an empty figure with no axes
# fig.suptitle('No axes on this figure')  # Add a title so we know which it is
 
 
success, segmented_images, splitted = ImageManager.get_n_segments(x, Commons.segments_number)
print(success, len(segmented_images))