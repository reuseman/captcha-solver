from __future__ import absolute_import, division, print_function

# TensorFlow and tf.keras
import tensorflow as tf
from tensorflow import keras
from tensorflow.train import Saver

# Helper libraries
import numpy as np
import matplotlib.pyplot as plt
import pickle

# system interface library
import os
from commons import Commons
from save_history import save_history
from copy import copy

# functions
import split_dataset


def train(train_dataset, modelname="nn", nlayer_units=[(28, 28), 128, 10], epochs=5, savepath=""):
    train_images = copy(train_dataset[0])
    train_labels = copy(train_dataset[1])

    #train_images = train_images > 0.5

    print('---------------')
    print(nlayer_units)
    print('---------------')
    # print(train_labels.shape)
    # print('---------------')

    if (modelname == "nn"):

        nn = keras.Sequential()
        nn.add(keras.layers.Flatten(input_shape=nlayer_units[0]))
        for i in range(1, len(nlayer_units)):
            if (i != (len(nlayer_units) - 1)):
                nn.add(keras.layers.Dense(
                    nlayer_units[i], activation=tf.nn.relu))
            else:
                nn.add(keras.layers.Dense(
                    nlayer_units[i], activation=tf.nn.softmax))

        print('---------------')
        print(nn.get_config())
        print('---------------')

        nn.compile(optimizer="adam",
                   loss="sparse_categorical_crossentropy", metrics=['accuracy'])

        nn.fit(train_images, train_labels, epochs=epochs)

        path = 'trained_models' if savepath == '' else savepath
        os.makedirs(path, exist_ok=True)
        nn.save('{path}/{modelname}-{units}-{epochs}.h5'.format(
            path=path, modelname=modelname, units=nlayer_units, epochs=epochs))


def evaluate_model(test_dataset, modelpath):
    if not(os.path.exists(modelpath)):
        raise FileNotFoundError('No model found at path: {}'.format(modelpath))
    if os.path.isdir(modelpath):
        models_list_path = [os.path.join(modelpath, model)
                            for model in os.listdir(modelpath)]

        timestamp_list = [os.path.getmtime(m) for m in models_list_path]

        max_timestamp_index = timestamp_list.index(max(timestamp_list))
        modelpath = models_list_path[max_timestamp_index]

    model = keras.models.load_model(modelpath)
    loss, accuracy = model.evaluate(test_dataset[0], test_dataset[1])

    return (loss, accuracy)


if __name__ == "__main__":


    # if not(os.path.exists(train_path)):
    # os.makedirs('test_data', exist_ok=True)
    #trainset, testset, evalset = split_dataset.split_dataset(
    #    label_map, image_height=30, image_width=30, trainset_percentage=90, testset_percentage=10)
#        print('---------------')
#        print(type(train_dataset[0]))
#        print('---------------')
#        print(type(train_dataset[1]))
#        print('---------------')

#
#    else:
#        if os.path.exists(train_path) and os.path.isfile(train_path):
#            with open(train_path, 'rb') as file:
#                print(train_path)
#                trainset = pickle.load(file)
#
#        if os.path.exists(test_path) and os.path.isfile(test_path):
#            with open(test_path, 'rb') as file:
#                testset = pickle.load(file)
#
#        if os.path.exists(evaluation_path) and os.path.isfile(evaluation_path):
#            with open(evaluation_path, 'rb') as file:
#                evaluationset = pickle.load(file)
#

#    print(len(label_map))
#    train_img = copy(trainset[0])
#    train_img = train_img > 0.5
#
#    plt.imshow(train_img[0])
#    plt.show()
#    class_names = ['T-shirt', 'Trouser', 'Pullover', 'Dress', 'Coat',
#                       'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']
#    fashion_mnist = keras.datasets.fashion_mnist
#
#    (train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data()
#    trainset = (train_images, train_labels)
#    testset = (test_images, test_labels)
#
    train_path = os.path.join(Commons.pickle_dataset_path, 'train_dataset')
    test_path = os.path.join(Commons.pickle_dataset_path, 'test_dataset')
    evaluation_path = os.path.join(
        Commons.pickle_dataset_path, 'evaluation_dataset')

    label_map = {}
    trainset, testset, evalset = split_dataset.split_dataset(
        label_map, image_height=30, image_width=30, trainset_percentage=90, testset_percentage=10)
#   
    train(trainset, epochs=5, nlayer_units=[
        (28, 28), 128, len(class_names)])

    loss, acc = evaluate_model(
        test_dataset=testset, modelpath='trained_models')
    print("Restored model, accuracy: {:5.2f}%".format(100*acc))



from os import listdir
from commons import Commons
from skimage.io import imread
import os
import numpy as np
from skimage.transform import resize
import matplotlib.pyplot as plt
import pickle


def split_dataset(label_map, trainset_percentage=80, testset_percentage=20, evaluationset_percentage=0, image_width=50, image_height=50, save=True):

    if (trainset_percentage + testset_percentage + evaluationset_percentage) != 100:
        print("[ERROR] split percentage not equal to 100, using trainset 80\% test 20\%")
        trainset_percentage = 80
        testset_percentage = 20
        evaluationset_percentage = 0

    segmented_dataset_path = Commons.segmented_dataset_path
    dataset = np.ndarray((2256, image_width, image_height))
    int_labels = np.ndarray((2256))

    label_list = listdir(segmented_dataset_path)

    # label map will contain a mapping from int to the string labels
    # like  {0 : 'a', 1: 'b', ... , ...}
    k = 0
    for i, label in enumerate(label_list):
        label_path = os.path.join(segmented_dataset_path, label)
        image_list = listdir(label_path)
        if len(image_list) == 0 :
            continue
        
        label_map[i] = label
        for j, image_name in enumerate(image_list):
            image_path = os.path.join(label_path, image_name)
            image = imread(image_path, as_gray=True)
            image = resize(image, (image_width, image_height),
                           anti_aliasing=True, cval=1)
            dataset[k] = image
            int_labels[k] = i
            k += 1

    num_trainset = round(k * (trainset_percentage/100))
    num_testset = round(k * (testset_percentage/100))
    num_evalset = round(k * (evaluationset_percentage/100))

    # shuffling let all the sets to have a randomly distributed
    # number of different letters
    np.random.seed(29)
    np.random.shuffle(dataset)
    np.random.seed(29)
    np.random.shuffle(int_labels)

    trainset = (
        np.array(dataset[0:num_trainset]), np.array(int_labels[0:num_trainset]))
    testset = (np.array(
        dataset[num_trainset: num_trainset + num_testset]),
        np.array(int_labels[num_trainset: num_trainset + num_testset]))
    evaluationset = (np.array(dataset[num_trainset + num_testset:k]),
                     np.array(
        int_labels[num_trainset + num_testset: k]))

    if save:
        savepath = Commons.pickle_dataset_path
        if not os.path.exists(savepath):
            os.makedirs(savepath)

        if trainset[0].shape[0] != 0:
            with open(os.path.join(savepath, 'train_dataset'), 'wb') as file:
                pickle.dump(trainset, file)
        elif os.path.exists( os.path.join(savepath, 'train_dataset')) :
            os.remove(os.path.join(savepath, 'train_dataset'))

        if testset[0].shape[0] != 0:
            with open(os.path.join(savepath, 'test_dataset'), 'wb') as file:
                pickle.dump(testset, file)
        elif os.path.exists( os.path.join(savepath, 'test_dataset')) :
            os.remove(os.path.join(savepath, 'test_dataset'))

        if evaluationset[0].shape[0] != 0:
            with open(os.path.join(savepath, 'evaluation_dataset'), 'wb') as file:
                pickle.dump(evaluationset, file)
        elif os.path.exists( os.path.join(savepath, 'evaluation_dataset')) :
            os.remove(os.path.join(savepath, 'evaluation_dataset'))

    return trainset, testset, evaluationset


if __name__ == "__main__":

    label_map = {}
    trainset, testset, evaluationset = split_dataset(label_map, save=True)
