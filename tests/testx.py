import matplotlib.pyplot as plt

from skimage.filters import threshold_minimum
from skimage.color import rgb2gray
import numpy as np
from skimage.io import imread
import matplotlib.image as mpimg
from scipy import ndimage as ndi
from skimage.filters import threshold_isodata, threshold_minimum, sobel
from skimage.measure import label, regionprops
from skimage.morphology import watershed, square, disk, erosion as erosion_filter
from skimage.filters.rank import median
from skimage.transform import resize

from managers.image_manager import ImageManager
from commons import Commons


def get_labeled_matrix(image):
    # Matrix of zeros with the same shape as the image, set value 1 if the original pixel is white, otherwise 2
    markers = np.zeros_like(image)
    markers[image > 0.1] = 1
    markers[image <= 0.1] = 2

    # Sobel use some sort of gradient and creates an elevation map that will be used to fill the different segments
    elevation_map = sobel(image)
    segmentation = watershed(elevation_map, markers)
    segmentation = ndi.binary_fill_holes(segmentation - 1)

    # Labeled is a matrix of the original image with 0 for the white, 1 to N for the different segments
    return markers, elevation_map, segmentation, label(segmentation)


def split_segments_with_two_letters(segments):
    # Obtain the index of the segment that contain two letters
    max_width_index, max_width = -1, -1
    for i, seg in enumerate(segments):
        if seg.shape[1] > max_width:
            max_width = seg.shape[1]
            max_width_index = i
    # Obtain the index of the column where the cut should be done.
    # Sum every column, and pick the one with the maximum value (more white pixels). A weight will be applied
    segment_to_cut = segments[max_width_index]
    sum_col = np.sum(segment_to_cut, axis=0)
    # Distance between the middle
    width = segment_to_cut.shape[1]
    mid_point = width // 2
    distances_from_mid_point = [abs(mid_point - 1 - n) for n in range(0, width)]
    distances_from_mid_point = np.multiply(distances_from_mid_point, 0.9)
    sum_col = np.subtract(sum_col, distances_from_mid_point)
    # Obtain the index of the column that contains more white pixels
    max_sum_value, max_sum_index = -1, -1
    for i, value in enumerate(sum_col):
        if value > max_sum_value:
            max_sum_value = value
            max_sum_index = i

    new_segments = []
    for i in range(0, len(segments)):
        if i != max_width_index:
            new_segments.append(segments[i])
        else:
            image1 = segments[i][:, :max_sum_index]
            image2 = segments[i][:, max_sum_index:]
            new_segments.append(median(image1, disk(1)))
            new_segments.append(median(image2, disk(1)))
    return new_segments, max_sum_index


binarized = imread("/home/alex/Development/python/captcha_solver/dataset/binarized/aggs.jpg", True)

success, s, splitted = ImageManager.get_n_segments(binarized, Commons.segments_number,
                                                                  debug_image_name="ci")

fig, ax = plt.subplots(1, 4)
ax[0].imshow(resize(s[0], (50, 50)), cmap="gray")
ax[0].set_title("a")

ax[1].imshow((s[1], (50, 50)), cmap="gray")
ax[1].set_title("g")

ax[2].imshow((s[2], (50, 50)), cmap="gray")
ax[2].set_title("g")

ax[3].imshow((s[3], (50, 50)), cmap="gray")
ax[3].set_title("s")

plt.tight_layout()

plt.show()
#plt.savefig("split.png", bbox_inches='tight')

# fig = plt.figure(figsize=(3, 1))  # an empty figure with no axes
# fig.suptitle('No axes on this figure')  # Add a title so we know which it is


success, segmented_images, splitted = ImageManager.get_n_segments(f, Commons.segments_number)
print(success, len(segmented_images))