from __future__ import absolute_import, division, print_function

# TensorFlow and tf.keras
import tensorflow as tf
from tensorflow import keras

# Helper libraries
import numpy as np
import matplotlib.pyplot as plt
from keras.utils import to_categorical
from copy import copy

fashion_mnist = keras.datasets.fashion_mnist
(train_images, train_labels), (test_images,
                               test_labels) = fashion_mnist.load_data()


train_images = train_images / 255.0
test_images = test_images / 255.0



model = keras.Sequential([
    keras.layers.Flatten(input_shape=(28, 28)),
    keras.layers.Dense(128, activation=tf.nn.relu),
    keras.layers.Dense(10, activation=tf.nn.softmax)
])


model.compile(optimizer='adam', 
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])


train_labels = np.array(copy(train_labels), dtype=np.dtype(np.uint8))
train_labels = train_labels.tolist()
train_categorical = to_categorical(
           train_labels, num_classes=10, dtype='uint8')


model.fit(train_images, train_categorical, epochs=1)
